export interface ApiStatus {
    status: 'ok'
    region: string
}

export interface Credentials {
    username: string
    password: string
}

export interface Token {
    token: string
}

export interface TokenPayload {
    u?: string
    sids: {[key: string]: Privacy}
}

export type Privacy = 'pb' | 'pr' | 'fr' | 'fm' | 'ff'

export interface Audience {
    a: 'fr' | 'fm'
}

const _anObjectWithThisLevelCanBeAccessedWithTokensOfThoseLevels: Record<Privacy, Array<Privacy>> = {
    'pr': ['pr'],
    'pb': ['pr', 'pb', 'fr', 'fm'],
    'fr': ['pr', 'fr'],
    'fm': ['pr', 'fm'],
    'ff': ['pr', 'fr', 'fm'],
};

export const canWrite = (token: TokenPayload, sid: string): boolean => {
    return typeof token.u !== 'undefined' && token.sids[sid] === 'pr';
};

export const canRead = (token: TokenPayload, sid: string, object: {d: {pr: Privacy}} | Privacy): boolean => {
    const tokenLevel = token.sids[sid] || 'pb';
    const objectLevel = typeof object === 'object' ? object.d.pr : object;
    return _anObjectWithThisLevelCanBeAccessedWithTokensOfThoseLevels[objectLevel].includes(tokenLevel);
};

export function slugify(str: string): string {
    return str
        .trim()
        .toLowerCase()
        .replace(/[\s/]+/g, '-')
        .replace(/ä/g, 'ae')
        .replace(/ö/g, 'oe')
        .replace(/ü/g, 'ue')
        .replace(/ß/g, 'ss')
        .replace(/[^\w\-]+/g, '')
        .replace(/--+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

export interface SiteList {
    ss: Array<SiteLink>
}

export interface SiteLink {
    sid: string
    d: SiteLinkData
}

export interface SiteLinkData {
    n: string
}

export interface SiteId {
    sid: string
}

export interface SiteVersion extends SiteId {
    v: number | undefined
}

export interface Site extends SiteVersion {
    d: SiteData
}

export interface SiteData {
    n: string
    as: Array<AlbumLink>
}

export interface AlbumLink {
    sid: string
    aid: string
    d: AlbumLinkData
    c: PhotoCount
    cv?: PhotoLink
}

export interface AlbumLinkData {
    n: string
    md: string
    pr: Privacy
}

export type PhotoCount = {
    pr?: number
    pb?: number
    fr?: number
    fm?: number
    ff?: undefined
}

export interface PhotoLink {
    fid: string
    n: string
    w: number
    h: number
    hx: string
}

export interface AlbumId {
    sid: string
    aid: string
}

export interface AlbumVersion extends AlbumId {
    v: number | undefined
}

export interface Album extends AlbumVersion {
    d: AlbumData
}

export interface AlbumData {
    es: Array<Element>
}

export type Element = PhotoElement | TextElement | MapElement;

export interface PhotoElement extends PhotoLink {
    t: 'p'
    ts: number
    d: PhotoElementData
}

export interface PhotoElementData {
    n: string
    md: string
    pr: Privacy
}

export interface TextElement {
    t: 't'
    d: TextElementData
}

export interface TextElementData {
    md: string
    a: 'left' | 'right' | 'center'
}

export interface MapElement {
    t: 'm'
    d: MapElementData
}

export interface MapElementData {
    l: Location
}

export interface Location {
    n: string
    bs: Array<GeoPoint>
}

export interface GeoPoint {
    lt: number
    lg: number
}

export interface UploadArtifact {
    ct: string
    n: string
    m: ArtifactMetadata
}

export interface ArtifactMetadata {
    original?: true
    w?: number
    h?: number
}

export interface UploadDestination {
    fid: string
    m: 'PUT'
    url: string
    hs: {[key: string]: string}
}

export interface File {
    sid: string
    fid: string
    v: number
    as: Array<StoredArtifact>
}

export interface StoredArtifact {
    n: string
    m: ArtifactMetadata
    o: string
    bs: Array<string>
}

export interface Feed {
    as: Array<{a: AlbumLink, es: PhotoElement[]}>
}
